<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c"  %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix = "fn"  uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>   
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Core Netowkrs Sevilla">
<meta name="description" content="Blog creado con Spring Boot, Spring MVC, JPA e Hibernate">

<title>Blog Curso Java Cloud</title>

<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"  rel="stylesheet">
<link href="assets/css/jumbotron-narrow.css"  rel="stylesheet">
</head>
<body>
  <div class="container">
    <div class="header clearfix">
	    <nav>
	     <ul  class="nav nav-pills pull-right">
		       <c:choose>
		         <c:when test="${not empty sessionScope.userLoggedIn }">
		            <jsp:include page="includes/menu_logged.jsp">
		            <jsp:param value="inicio" name="inicio"/>
		            <jsp:param name="usuario" value="${sessionScope.userLoggedIn.nombre }"/>
		            
		          </jsp:include>
		         </c:when>
		         <c:otherwise>
		          <jsp:include page="includes/menu.jsp">
		            <jsp:param value="inicio" name="inicio"/>
		          </jsp:include>
		         </c:otherwise> 
		       </c:choose>  
	       </ul>
	    </nav>
	    <h3 class="text-muted">Blog Core</h3>
    
    </div>
    
    <div class="col-lg-8 col-lg-offset-2" >
    
       <div class="text-center">
          <h3>Escribe Una Entrada</h3>
          <form:form id="register-form"  action="/submit/newpost" method="post" role="form" 
            autocomplete="off" modelAttribute="post"
          >
          
          
           <div class="form-group">
           
              <form:input type="text" name="titulo" id="titulo" tabIndex="1" class="form-control"   path="titulo" placeholder="Titulo Entrada"/>
           </div>
           
           <div class="form-group">
              <form:input type="email" name="url" id="url" tabIndex="2" class="form-control"   path="url" placeholder="Url (Opcional..)" />
           </div>
           
           <div class="form-group">
              <form:textarea type="text" name="contenido" id="contenido" rows="10" tabIndex="3" class="form-control textarea"   path="contenido" placeholder="Texto del Post" />
           </div>
           
 
   
           </div>          
            <div class="form-group">
              <input type="submit" name="register-submit" id="register-submit" tabIndex="6" class="form-control btn btn-info"  value="Subir Post!" /> 
              
           </div>          
          </form:form>
       </div>
    
    
    </div>

  
  
  </div>
  
  
  <script src="webjars/jquery/3.1.1/jquery.min.js"></script> 
  <script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
  <script src="assets/js/jquery.validate.min.js"></script>
  <script src="assets/js/messages_es.js"></script>
  <script>
    $(document).ready(function() { 
              $("#register-form").validate();
         }
     );
     
  </script>
</body>
</html>