<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c"  %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix = "fn"  uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>   
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="author" content="Core Netowkrs Sevilla">
<meta name="description" content="Blog creado con Spring Boot, Spring MVC, JPA e Hibernate">

<title>Blog Curso Java Cloud</title>

<link href="webjars/bootstrap/3.3.7-1/css/bootstrap.min.css"  rel="stylesheet">
<link href="assets/css/jumbotron-narrow.css"  rel="stylesheet">
</head>
<body>
  <div class="container">
    <div class="header clearfix">
	    <nav>
	       <ul  class="nav nav-pills pull-right">
		       <c:choose>
		         <c:when test="${not empty sessionScope.userLoggedIn }">
		            <jsp:include page="includes/menu_logged.jsp">
		            <jsp:param value="inicio" name="inicio"/>
		            <jsp:param name="usuario" value="${sessionScope.userLoggedIn.nombre }"/>
		            
		          </jsp:include>
		         </c:when>
		         <c:otherwise>
		          <jsp:include page="includes/menu.jsp">
		            <jsp:param value="inicio" name="inicio"/>
		          </jsp:include>
		         </c:otherwise> 
		       </c:choose>   
	       </ul>
	       
	    </nav>
	    <h3 class="text-muted">Blog Core</h3>
    
    </div>
    
 	    <div class="jumbotron">
          <h1>CoreBlog, el blog sobre Hibernate</h1>
          <p>Tutoriales, información y tips sobre Hibernate.</p>
          <c:if test="${empty sessionScope.userLoggedIn }">
               <p><a href="/signup"  role="button"  class="btn btn-lg btn-success ">Regístrate</a></p>
		  </c:if>
        </div>   
        
        <c:forEach items="${listaPost}" var="postItem">
       <div class="container">
    <div class="well"> 
        <div class="row">
             <div class="col-md-12">
                 <div class="row hidden-md hidden-lg"><h1 class="text-center" ><a href="/post/${postItem.id}">${postItem.titulo}</a></h1></div>
                     
                 <div class="pull-left col-md-4 col-xs-12 thumb-contenido"><img class="center-block img-responsive" src='http://placeimg.com/500/500/tech' /></div>
                 <div class="">
                     <h1  class="hidden-xs hidden-sm">${postItem.titulo}</h1>
                     <hr>
                     <br/>
                     <span class="badge">Escrito el <fmt:formatDate pattern="dd/MM/yyyy" value="${postItem.fecha}" /> a las 
					<fmt:formatDate pattern="HH:mm:ss" value="${postItem.fecha}" /></span>
                     
                     <small><strong>${postItem.autor.nombre}</strong></small>
                     <hr>
                     <p class="text-justify">${postItem.contenido}</p></div>
                     <span class="label label-info">
					    ${(fn:length(postItem.comments) gt 0) ? fn:length(postItem.comments) : 'Sin '} 
						${(fn:length(postItem.comments) == 1) ? 'comentario' : 'comentarios'} 
						
						</span>
             </div>
        </div>
    </div>
</div>
        </c:forEach>
        
  
  
  </div>
  
  
  <script src="webjars/jquery/3.1.1/jquery.min.js"></script> 
  <script src="webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>
  

  
</body>
</html>