package com.corenetworks.hibernate.blog.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.blog.model.Post;

@Repository
@Transactional
public class PostDao {
	
    @PersistenceContext
	private EntityManager entityManager;
    
    /*
     * Almacena el post en la base de datos
     */
    public void create(Post post) {
    	entityManager.persist(post);
    	return;
    }
    
    @SuppressWarnings("unchecked")
	public List<Post> getAll(){
    	return entityManager
    			.createQuery("select p from Post p")
    			.getResultList();
    }
    
    /**
     * EntityManager busca en la base de datos un post con una id introducida
     * para retornar un post segun la id.
     * 
     * @param id la ID que se busca.
     * @return retorna un Post con un atributo ID dado.
     * 
     * */
    public Post getById(long id) {
    		return entityManager.find(Post.class, id);
    }
 
    
    
    
}
