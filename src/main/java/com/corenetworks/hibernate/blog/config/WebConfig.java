package com.corenetworks.hibernate.blog.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/*
 * Mcv -> Modelo vista controlador. Importante.
 * 
 * */


@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {
	/*
	 * Cuando alguien coloque esa url le diremos que realmente
	 * se encuentra en /META-INF/resource/webjars/
	 * Lo que hace es autocompletar realmente. 
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry
			.addResourceHandler("/webjars/**")
			.addResourceLocations("classpath:/META-INF/resources/webjars/");
		registry
			.addResourceHandler("/assets/**")
			.addResourceLocations("/assets/");
		
		}
	
	@Bean
	public InternalResourceViewResolver getViewResolver() {
		InternalResourceViewResolver resolver
			= new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/jsp/");
		resolver.setSuffix(".jsp");
		return resolver;
	}
	
	}
