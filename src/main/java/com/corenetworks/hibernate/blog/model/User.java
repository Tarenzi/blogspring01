package com.corenetworks.hibernate.blog.model;

import java.util.HashSet;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.CreationTimestamp;

@Entity
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	@Column(name="nombre")
	private String nombre;
	
	@Column
	private String email;
	
	@Column
	@CreationTimestamp
	private Date fechaAlta;
	
	@Column
	private String ciudad;
	
	@Column
	@ColumnTransformer(write=" MD5(?) ")
	private String password;

	@OneToMany(mappedBy="user")
	private Set<Comment> comments = new HashSet<>();
	
	@OneToMany
	private Set<Post> posts = new HashSet<>();
	
	public User() {
	}	
	
	public User(String nombre, String email, String ciudad, String password) {
		
		this.nombre = nombre;
		this.email = email;
		this.ciudad = ciudad;
		this.password = password;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Date fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", nombre=" + nombre + ", email=" + email + ", fechaAlta=" + fechaAlta + ", ciudad="
				+ ciudad + ", password=" + password + "]";
	}
	
	
	

}
