package com.corenetworks.hibernate.blog.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity
public class Comment {

	@Id
	@GeneratedValue
	private long id;
	
	/*
	 *  //La entidad Comment posee en sus atributos una entidad usuario a la que estará asociada.
	 * 
	 * @ManyToOne define la asociacion con otra entidad (en este caso es la entidad User) de muchos a uno solo.
	 * Es decir, muchas entidades comentario para una sola entidad usuario.
	 * 
	 * @JoinColumn especifica una columna para unir una asociacion con una entidad o elemento de coleccion.
	 * en este caso asignamos un nombre que será user_id(la columna) asociada a la entidad User.
	 * updatable debe de estar en falso para evitar actualizaciones..
	 * */
	@ManyToOne
	@JoinColumn(name="user_id", updatable=false)
	private User user;
	
	
	/*
	 * //La entidad Comment posee en sus atributos un Post al que estará asociado.
	 * 
	 * @ManyToOne muchas entidades comentario para una sola entidad Post.
	 * 
	 * @JoinColumn especifica una columna para unir una asociacion con la entidad Post.
	 * En este caso asignamos un nombre que será "post_id" para la columna de la entidad, la cual
	 * estará asociada a la entidad Post.
	 * 
	 * las actualizaciones de la columna estan desactivadas.
	 * 
	 * */
	@ManyToOne
	@JoinColumn(name="post_id", updatable=false)
	private Post post;
	
	/*
	 * Esto poseerá el contenido del comentario con una persistencia de tamaño grande.
	 * La etiqueta @Lob especifica que la persistencia en la base de datos
	 * va a ser la de un objeto de tamaño grande.*/
	@Column
	@Lob
	private String contenido;
	
	@Column
	@CreationTimestamp
	private Date date;

	public Comment(User user, Post post, String contenido) {
		super();
		this.user = user;
		this.post = post;
		this.contenido = contenido;
	}
	
	public Comment() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}
