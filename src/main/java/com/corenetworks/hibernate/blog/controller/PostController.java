package com.corenetworks.hibernate.blog.controller;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.hibernate.blog.beans.PostBean;
import com.corenetworks.hibernate.blog.dao.PostDao;
import com.corenetworks.hibernate.blog.model.Post;
import com.corenetworks.hibernate.blog.model.User;


@Controller
public class PostController {
	@Autowired
	private PostDao postDao;
	
	//HttpSession para rescatar la sesion del autor.
	@Autowired
	private HttpSession httpSession;
	
	/**
	 * showForm es invocado por el controlador cuando el cliente accede a /submit
	 * 
	 * Permite mostrar los post, añadiendo al modelo el post.
	 * 
	 */
	@GetMapping(value = "/submit")
	public String showForm(Model modelo) {
		modelo.addAttribute("post", new PostBean());
		return "submit";
	}
	
	
	/**
	 * Submit es invocado por el controlador cuando el cliente accede a /submit/newpost
	 * 
	 * Permite crear un nuevo post, asociandolo a su autor, del cual se rescata
	 * su sesion para asignarlo como autor de dicho post.
	 * 
	 * Para ello, primero crea un objeto de tipo Post para asignarle los atributos del
	 * objeto PostBean.
	 * Después rescata la sesion de usuario utilizando HttpSession y asignando el objeto devuelto
	 * a un objeto Usuario llamado autor.
	 * 
	 * Por último añade al post el autor rescatado gracias a HttpSession y hace persistente el post
	 * utilizando el Objeto DAO llamado PostDao.
	 * 
	 * @param r el bean de Post
	 * @param model el modelo.
	 * @return una redireccion hacia la raiz.
	 * */
	@PostMapping(value = "/submit/newpost")
	public String submit(@ModelAttribute("post") PostBean r, Model model) {
		
         // Crear un Post
		
		/*Nuevo objeto Post usando los Sets para asignarle sus atributos. (el atributo de Usuario aun no puesto
		 que aun no rescatamos la sesion del usuario).*/
		Post post = new Post();
		post.setTitulo(r.getTitulo());
		post.setContenido(r.getContenido());
		post.setUrl(r.getUrl());
		
		 // Obtener el autor desde la sesión mediante la sesion.
		
		/*Obtenemos de esta manera la sesion del usuario, casteando el objeto devuelto por httpSession al
		  tipado User.*/
		User autorPost = (User) httpSession.getAttribute("userLoggedIn");
		
		 // Asignar el autor al post ya creado usando el SetAutor.
		post.setAutor(autorPost);
		
		//Hacemos persistente el post utilizando el Dao para acceder a la base de datos
		postDao.create(post);

		//A continuacion redireccionamos al usuario al directorio raiz.
		
		return "redirect:/";
	}	
	
	@GetMapping(value = "/post/{id}")
	public String detail(@PathVariable("id")  long id,   Model modelo) {
		//modelo.addAttribute("post", new PostBean());
		// Comprobar si el Post existe.
		Post result = null;
		
		if((result = postDao.getById(id)) != null) {
			modelo.addAttribute("post", result);
			return "postdetails";
		} else {
			return "redirect:/";
		}
		
	}
}
