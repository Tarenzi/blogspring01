package com.corenetworks.hibernate.blog.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.hibernate.blog.beans.LoginBean;

import com.corenetworks.hibernate.blog.dao.UserDao;
import com.corenetworks.hibernate.blog.model.User;

@Controller
public class LoginController {
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private HttpSession httpSession;

	@GetMapping(value = "/signin")
	public String showForm(Model model) {
		model.addAttribute("userLogin", new LoginBean());
		return "login";
	}

	@PostMapping(value = "/login")
	public String submit(@ModelAttribute("userLogin") LoginBean loginBean, Model model) {
		// userDao.create(new User(r.getNombre(), r.getEmail(), r.getCiudad(),
		// r.getPassword()));
		User u = userDao.getByEmailAndPassword(loginBean.getEmail(), loginBean.getPassword());
		if (u != null) {
			httpSession.setAttribute("userLoggedIn", u);
			
		    return "redirect:/";
		} else {
			model.addAttribute("error","Error de validación");
			return "login";
		}
	}
	
	
	@GetMapping(value = "/logout")
	public String logout(Model model) {
		httpSession.removeAttribute("userLoggedIn");
		return "redirect:/";
	}
}
