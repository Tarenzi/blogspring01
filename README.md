# README #

En este Blog iremos poniendo en pr�ctica todo lo que vayamos aprendiendo en el curso de programaci�n de aplicaciones Java SE 8 para su implementaci�n en Cloud.

### �Qu� es este BitBucket? ###

* Aqu� ponemos en pr�ctica todo lo que aprendemos sobre Java SE 8.
* Ampliaremos nuestros conocimientos.
* Una forma de dar un vistazo a todo lo que s� y lo que aprend� para poder seguir evolucionando.

### �Qu� es lo que aprendemos? ###

* Impartimos los cursos de [Oracle](https://www.oracle.com/es/index.html): Java SE 8 Fundamentals y Java SE 8 Programming.
* El desarrollo de proyectos con [Spring](https://spring.io/)
* Caracteristicas del framework MVC (modelo-vista-controlador) que incorpora [Spring](https://spring.io/).
* Empleo de la herramienta Hibernate.
* Desarrollo de proyectos Java con Struts.
* Implementacion en la nube de proyectos Java.
* Hemos aprendido el manejo de Beans.
* Manejo de IDEs como NetBeans y Eclipse.
* Un primer acercamiento a herramientas como Github y Bitbucket.
* Manejo del modelo-vista-controlador
* Creacion de Entity's (entidades) para hacer persistentes los POJOs.
* Creacion de DAO's para permitir que los objetos realicen cambios en la base de datos.
* Creacion de Controllers para que el cliente pueda realizar peticiones a los DAO.
* Recuperacion de sesiones http con la ayuda de HttpSession.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

